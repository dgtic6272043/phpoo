<?php

// Declaracion de la clase carro

class Carro {
    public $color; // Atributo color
    public $marca; // Atributo marca
    public $modelo; // Atributo modelo

// Aplicacion del constructor para inicializar los atributos

    public function __construct($color = '', $marca = '', $modelo = '') {
        $this->color = $color;
        $this->marca = $marca;
        $this->modelo = $modelo;
    }
}

// Inicializar la variable del mensaje con una cadena vacia

$mensajeServidor = '';

// Agregamos un if para validar que se hayan enviado los datos correctamente

if (!empty($_POST) && isset($_POST['color'], $_POST['marcaCarro'], $_POST['modeloCarro'])) {

    // Si sí, se modifica en mensaje del servidor para encadenar las variables como texto

    $mensajeServidor = 'Has elegido un carro con marca: ' . $_POST['marcaCarro'] . ', modelo: ' . $_POST['modeloCarro'] . ' y color: ' . $_POST['color'];
}

?>