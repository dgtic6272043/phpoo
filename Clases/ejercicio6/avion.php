<?php  
// Incluimos el archivo trnasporte.php del que vamos a heredar
include_once('transporte.php');

// Hacemos que la classe avion herede de transporte

class avion extends transporte {

    private $numero_turbinas; //Atributo num tubinas
    
    //Sobreescribimos en el constructor y agregamos el atributo turbinas

    public function __construct($nom,$vel,$com,$tur){
        parent::__construct($nom,$vel,$com);
        $this->numero_turbinas=$tur;
    }

    // Creamos el metodo resumenAvion
    public function resumenAvion(){
        $mensaje=parent::crear_ficha();
        $mensaje.='<tr>
                    <td>Numero de turbinas:</td>
                    <td>'. $this->numero_turbinas.'</td>                
                </tr>';
        return $mensaje;
    }
}
?>
