<?php  
//Incluimos el archivo del que vamos a heredar
include_once('transporte.php');

//Heredamos para carro los atributos de transporte
class carro extends transporte {

    private $numero_puertas;

    //Sobreescribimos el constructor para agregar puertas
    public function __construct($nom,$vel,$com,$pue){
        parent::__construct($nom,$vel,$com);
        $this->numero_puertas=$pue;        
    }

    // Crwamos el metodo resumenCarro 
    public function resumenCarro(){
        $mensaje=parent::crear_ficha();
        $mensaje.='<tr>
                    <td>Numero de puertas:</td>
                    <td>'. $this->numero_puertas.'</td>                
                </tr>';
        return $mensaje;
    }
}
?>
