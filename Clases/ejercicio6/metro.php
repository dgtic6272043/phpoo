<?php  
//Incluimos el archivo del que vamos a heredar
include_once('transporte.php');

//Creamos la clase metro que hereda de transporte
class metro extends transporte {
    private $numero_vagones; // Atributo vagones

    // Sobreescribimos el constructor para agregar el atributo vagones
    public function __construct($nom,$vel,$com,$vagones){
        parent::__construct($nom,$vel,$com);
        $this->numero_vagones = $vagones;
    }
    
    //Creamos el metodo resumenMetro
    public function resumenMetro(){
        $mensaje = parent::crear_ficha();
        $mensaje .= '<tr>
                    <td>Número de vagones:</td>
                    <td>'. $this->numero_vagones.'</td>                
                </tr>';
        return $mensaje;
    }
}
?>
